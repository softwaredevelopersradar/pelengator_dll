﻿namespace Pelengator_DLL
{
    /// <summary>
    /// Класс констант
    /// </summary>
    public class PelengatorConts
    {
        /// <summary>
        /// Результат регистрации
        /// </summary>
        public const string OkRegResult = "OkRes";

        /// <summary>
        /// Имя поста-отправителя
        /// </summary>
        public const string Sender = "client";

        /// <summary>
        /// Имя поста-пеленгатора
        /// </summary>
        public const string Dest = "server";

        /// <summary>
        /// Формат времени
        /// </summary>
        public const string DateTimeFormat = "dd.MM.yyyy HH:mm:s";

        /// <summary>
        /// Xml элемент
        /// </summary>
        public const string XmlEofMarker = "</unp>";
    }
}
