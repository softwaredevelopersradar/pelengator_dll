﻿using System.Xml.Serialization;

namespace Pelengator_DLL
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "fpp")]
    public class DirFpp
    {
        /// <summary>
        /// Начало диапазона (МГц)
        /// </summary>
        [XmlAttribute(AttributeName = "bfreq")]
        public float BFreq { get; set; }

        /// <summary>
        /// Конец диапазона (МГц)
        /// </summary>
        [XmlAttribute(AttributeName = "efreq")]
        public float EFreq { get; set; }

        /// <summary>
        /// Идентификатор задачи Поиска
        /// </summary>
        [XmlAttribute(AttributeName = "id")]
        public float Id { get; set; }

        /// <summary>
        /// Cпектральный шаг (МГц)
        /// </summary>
        [XmlAttribute(AttributeName = "step")]
        public float Step { get; set; }

        /// <summary>
        /// Количество отсчетов в массивах (уровня, азимута)
        /// </summary>
        [XmlAttribute(AttributeName = "N")]
        public int N { get; set; }

        /// <summary>
        /// String Массив уровней 
        /// </summary>
        [XmlAttribute(AttributeName = "levels")]
        public string Levels { get; set; }

        /// <summary>
        /// String Массив азимутов 
        /// </summary>
        [XmlAttribute(AttributeName = "azs")]
        public string Azs { get; set; }

        /// <summary>
        /// Массив уровней 
        /// </summary>
        public double[] LevelsArr { get; set; }

        /// <summary>
        /// Массив азимутов 
        /// </summary>
        public double[] AzsArr { get; set; }
    }

    /// <summary>
    /// Класс для комманды результат чпп
    /// </summary>
    [XmlRoot(ElementName = "fppresult")]
    public class Fppresult
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = "fpp")]
        public DirFpp Dir { get; set; }
        
        /// <summary>
        /// Имя поста-пеленгатора
        /// </summary>
        [XmlAttribute(AttributeName = "sender")]
        public string Sender { get; set; }

        /// <summary>
        /// Имя поста-источника команды
        /// </summary>
        [XmlAttribute(AttributeName = "dest")]
        public string Dest { get; set; }

        /// <summary>
        /// Номер команды
        /// </summary>
        [XmlAttribute(AttributeName = "qdata")]
        public int Qdata { get; set; }
    }
}
