﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Pelengator_DLL
{
    /// <summary>
    /// Класс для комманды отложенное пеленгование 
    /// </summary>
    [XmlRoot(ElementName = "ldftask")]
    public class Ldftask
    {
        /// <summary>
        /// Частота
        /// </summary>
        [XmlElement(ElementName = "freqs")]
        public Freqs Freqs { get; set; }

        /// <summary>
        /// Имя поста-отправителя
        /// </summary>
        [XmlAttribute(AttributeName = "sender")]
        public string Sender { get; set; }

        /// <summary>
        /// Имя поста-пеленгатора
        /// </summary>
        [XmlAttribute(AttributeName = "dest")]
        public string Dest { get; set; }

        /// <summary>
        /// Номер команды
        /// </summary>
        [XmlAttribute(AttributeName = "qdata")]
        public int Qdata { get; set; }

        /// <summary>
        /// ID
        /// </summary>
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }

        /// <summary>
        /// Active
        /// </summary>
        [XmlAttribute(AttributeName = "Active")]
        public string Active { get; set; }
    }
}
