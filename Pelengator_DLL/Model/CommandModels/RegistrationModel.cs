﻿using System.Xml.Serialization;

namespace Pelengator_DLL
{
    /// <summary>
    /// Ответ на регистрацию
    /// </summary>
    [XmlRoot(ElementName = "unpRegister")]
    public class ConnectMessage : RequestMessageBase
    {
        /// <summary>
        /// Имя регистрирующего поста
        /// </summary>
        [XmlAttribute(AttributeName = "hostname")]
        public string Hostname { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        [XmlAttribute(AttributeName = "password")]
        public string Password { get; set; }

        /// <summary>
        /// Версия
        /// </summary>
        [XmlAttribute(AttributeName = "ver")]
        public string Ver { get; set; }

        /// <summary>
        /// Имя регистрируемого поста
        /// </summary>
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Результат запроса
        /// </summary>
        [XmlAttribute(AttributeName = "result")]
        public string Result { get; set; }

        /// <summary>
        /// Результат регистрации
        /// </summary>
        [XmlAttribute(AttributeName = "regresult")]
        public string Regresult { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        [XmlAttribute(AttributeName = "status")]
        public string Status { get; set; }
    }

    /// <summary>
    /// Общий класс
    /// </summary>
    [XmlRoot(ElementName = "unp")]
    public class Unp
    {
        /// <summary>
        /// Получение кода команды 
        /// </summary>
        /// <returns>Код команды</returns>
        public PelengatorResponceType GetMessageType()
        {
            if (this.Dfresult != null)
                return PelengatorResponceType.PELENG_RESULT;

            if (this.Ldfresult != null)
                return PelengatorResponceType.DELAYED_PELENG_RESULT;

            if (this.Dftask != null && this.Dftask.Active == "True")
                return PelengatorResponceType.PELENG_STARTED;

            if (this.Dftask != null && this.Dftask.Active != "True")
                return PelengatorResponceType.PELENG_EOF;

            if (this.Ldftask != null && this.Ldftask.Active == "True")
                return PelengatorResponceType.DELAYED_PELENG_STARTED;

            if (this.Ldftask != null && this.Ldftask.Active != "True")
                return PelengatorResponceType.DELAYED_PELENG_EOF;

            if (UnpRegister != null)
                return PelengatorResponceType.REGISTRATION_RESPONSE;

            if (Fppresult != null)
                return PelengatorResponceType.FPP_RESULT;

            return PelengatorResponceType.UNDEFINED;
        }

        /// <summary>
        /// Запрос регистрации
        /// </summary>
        [XmlElement(ElementName = "unpRegister")]
        public ConnectMessage UnpRegister { get; set; }

        /// <summary>
        /// Команда на пеленгование
        /// </summary>
        [XmlElement(ElementName = "dftask")]
        public Dftask Dftask { get; set; }

        /// <summary>
        /// Команда на отложенное пеленгование
        /// </summary>
        [XmlElement(ElementName = "ldftask")]
        public Ldftask Ldftask { get; set; }

        /// <summary>
        /// Результат пеленгования
        /// </summary>
        [XmlElement(ElementName = "dfresult")]
        public Dfresult Dfresult { get; set; }

        /// <summary>
        /// Результат пеленгования
        /// </summary>
        [XmlElement(ElementName = "ldfresult")]
        public Ldfresult Ldfresult { get; set; }

        /// <summary>
        /// Ответ ЧПП
        /// </summary>
        [XmlElement(ElementName = "fppresult")]
        public Fppresult Fppresult { get; set; }

        /// <summary>
        /// Запрос ЧПП
        /// </summary>
        [XmlElement(ElementName = "fpptask")]
        public Fppftask Fppftask  { get; set; }
    }
}
