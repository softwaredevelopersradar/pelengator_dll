﻿using System.Xml.Serialization;

namespace Pelengator_DLL
{
    /// <summary>
    /// Запрос ЧПП
    /// </summary>
    [XmlRoot(ElementName = "fpptask")]
    public class Fppftask
    {
        /// <summary>
        /// Имя поста-отправителя
        /// </summary>
        [XmlAttribute(AttributeName = "sender")]
        public string Sender { get; set; }

        /// <summary>
        /// Имя поста-пеленгатора
        /// </summary>
        [XmlAttribute(AttributeName = "dest")]
        public string Dest { get; set; }

        /// <summary>
        /// Номер команды
        /// </summary>
        [XmlAttribute(AttributeName = "qdata")]
        public int Qdata { get; set; }
    }
}
