﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Pelengator_DLL
{
    /// <summary>
    /// Класс для комманды результат пеленгования
    /// </summary>
    [XmlRoot(ElementName = "ldfresult")]
    public class Ldfresult
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = "dir")]
        public List<Dir> Dir { get; set; }

        /// <summary>
        /// Version
        /// </summary>
        [XmlAttribute(AttributeName = "ver")]
        public string Ver { get; set; }

        /// <summary>
        /// Имя поста-пеленгатора
        /// </summary>
        [XmlAttribute(AttributeName = "sender")]
        public string Sender { get; set; }

        /// <summary>
        /// Имя поста-источника команды
        /// </summary>
        [XmlAttribute(AttributeName = "dest")]
        public string Dest { get; set; }

        /// <summary>
        /// Номер команды
        /// </summary>
        [XmlAttribute(AttributeName = "qdata")]
        public int Qdata { get; set; }

        /// <summary>
        /// 0
        /// </summary>
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }
}

