﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Pelengator_DLL
{
    /// <summary>
    /// Общий класс для тега band для двух видов пеленгования  
    /// </summary>
    [XmlRoot(ElementName = "band")]
    public class Band
    {
        /// <summary>
        /// Частота (Гц) (100 000 – 30 000 000)
        /// </summary>
        [XmlAttribute(AttributeName = "base")]
        public long Base { get; set; }

        /// <summary>
        /// Полоса (Гц) (300 – 12 000)
        /// </summary>
        [XmlAttribute(AttributeName = "width")]
        public int Width { get; set; }

        /// <summary>
        /// Время пеленгования (отложенный режим) в формате ДД.ММ.ГГГГ ЧЧ:ММ:СС
        /// </summary>
        [XmlAttribute(AttributeName = "time")]
        public string Time { get; set; }

        /// <summary>
        /// Длительность в секундах
        /// </summary>
        [XmlAttribute(AttributeName = "period")]
        public double Period { get; set; }

        /// <summary>
        /// 0/1 – передавать спектр?
        /// </summary>
        [XmlAttribute(AttributeName = "spectr")]
        public int Spectr { get; set; }

        /// <summary>
        /// 0/1 – передавать динамику параметров?
        /// </summary>
        [XmlAttribute(AttributeName = "dynamic")]
        public int Dynamic { get; set; }

        /// <summary>
        /// 0/1 – передавать спектр?
        /// </summary>
        [XmlAttribute(AttributeName = "all_iri")]
        public int All_iri { get; set; }

    }

    /// <summary>
    /// Общий класс тега freqs для двух видов пеленгования  
    /// </summary>
    [XmlRoot(ElementName = "freqs")]
    public class Freqs
    {
        /// <summary>
        /// Ответ пеленга
        /// </summary>
        [XmlElement(ElementName = "band")]
        public List<Band> Band { get; set; }
    }

    /// <summary>
    /// Класс для комманды отложенное пеленгование 
    /// </summary>
    [XmlRoot(ElementName = "dftask")]
    public class Dftask
    {
        /// <summary>
        /// Частота
        /// </summary>
        [XmlElement(ElementName = "freqs")]
        public Freqs Freqs { get; set; }

        /// <summary>
        /// Имя поста-отправителя
        /// </summary>
        [XmlAttribute(AttributeName = "sender")]
        public string Sender { get; set; }

        /// <summary>
        /// Имя поста-пеленгатора
        /// </summary>
        [XmlAttribute(AttributeName = "dest")]
        public string Dest { get; set; }

        /// <summary>
        /// Номер команды
        /// </summary>
        [XmlAttribute(AttributeName = "qdata")]
        public int Qdata { get; set; }

        /// <summary>
        /// Версия
        /// </summary>
        [XmlAttribute(AttributeName = "ver")]
        public int Ver { get; set; }

        /// <summary>
        /// ID
        /// </summary>
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }

        /// <summary>
        /// Active
        /// </summary>
        [XmlAttribute(AttributeName = "active")]
        public string Active { get; set; }
    }
}
