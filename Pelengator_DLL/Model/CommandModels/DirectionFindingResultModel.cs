﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Pelengator_DLL
{
    /// <summary>
    /// Результат пеленга
    /// </summary>
    [XmlRoot(ElementName = "dir")]
    public class Dir
    {
        /// <summary>
        /// Имя поста, на котором были получены результаты
        /// </summary>
        [XmlAttribute(AttributeName = "post")]
        public string Post { get; set; }

        /// <summary>
        /// Частота РПУ (Гц)
        /// </summary>
        [XmlAttribute(AttributeName = "freq")]
        public long Freq { get; set; }

        /// <summary>
        /// Время выполнения задачи в формате ДД.ММ.ГГГГ ЧЧ:ММ:СС
        /// </summary>
        [XmlAttribute(AttributeName = "time")]
        public string Time { get; set; }

        /// <summary>
        /// Эквивалент качества (достоверность пеленга за период пеленгования) (массив)
        /// </summary>
        [XmlAttribute(AttributeName = "qual")]
        public string Qual { get; set; }

        /// <summary>
        /// Широта пеленгатора (00:00:00.0N)
        /// </summary>
        [XmlAttribute(AttributeName = "lat")]
        public string Lat { get; set; }

        /// <summary>
        /// Долгота пеленгатора (00:00:00.0E)
        /// </summary>
        [XmlAttribute(AttributeName = "lon")]
        public string Lon { get; set; }

        /// <summary>
        /// Пеленг, гр. 
        /// </summary>
        [XmlAttribute(AttributeName = "hdir")]
        public string Hdir { get; set; }

        /// <summary>
        /// СКО азимута, гр. (массив)
        /// </summary>
        [XmlAttribute(AttributeName = "hdir_sko")]
        public string Hdir_sko { get; set; }

        /// <summary>
        /// Угол места, гр.
        /// </summary>
        [XmlAttribute(AttributeName = "vdir")]
        public string Vdir { get; set; }

        /// <summary>
        /// СКО угла места, гр. (массив)
        /// </summary>
        [XmlAttribute(AttributeName = "vdir_sko")]
        public string Vdir_sko { get; set; }

        /// <summary>
        /// Дальность, км (опционально)
        /// </summary>
        [XmlAttribute(AttributeName = "dist")]
        public string Dist { get; set; }

        /// <summary>
        /// Уровень сигнала (дБ/мкВ)
        /// </summary>
        [XmlAttribute(AttributeName = "pwr")]
        public string Pwr { get; set; }

        /// <summary>
        /// Массив динамики азимута
        /// </summary>
        [XmlAttribute(AttributeName = "hdir_dyn")]
        public string Hdir_dyn { get; set; }

        /// <summary>
        /// Массив динамики угла места
        /// </summary>
        [XmlAttribute(AttributeName = "vdir_dyn")]
        public string Vdir_dyn { get; set; }

        /// <summary>
        /// Массив динамики уровня
        /// </summary>
        [XmlAttribute(AttributeName = "pwr_dyn")]
        public string Pwr_dyn { get; set; }

        /// <summary>
        /// Массив времени
        /// </summary>
        [XmlAttribute(AttributeName = "time_dyn")]
        public string Time_dyn { get; set; }

        /// <summary>
        /// Начальная частота в спектре (Гц)
        /// </summary>
        [XmlAttribute(AttributeName = "spectr_f0")]
        public string Spectr_f0 { get; set; }

        /// <summary>
        /// Шаг спектра (Гц)
        /// </summary>
        [XmlAttribute(AttributeName = "spectr_step")]
        public string Spectr_step { get; set; }

        /// <summary>
        /// Массив уровней (дБ)
        /// </summary>
        [XmlAttribute(AttributeName = "spectr_u")]
        public string Spectr_u { get; set; }

        /// <summary>
        /// Класс сигнала (опционально) 
        /// </summary>
        [XmlAttribute(AttributeName = "klass")]
        public string Klass { get; set; }

        /// <summary>
        /// СКК сигнала (опционально)
        /// </summary>
        [XmlAttribute(AttributeName = "scc")]
        public string Scc { get; set; }

    }

    /// <summary>
    /// Класс для комманды результат пеленгования
    /// </summary>
    [XmlRoot(ElementName = "dfresult")]
    public class Dfresult
    {
        /// <summary>
        /// Результат пеленга
        /// </summary>
        [XmlElement(ElementName = "dir")]
        public List<Dir> Dir { get; set; }

        /// <summary>
        /// Version
        /// </summary>
        [XmlAttribute(AttributeName = "ver")]
        public string Ver { get; set; }

        /// <summary>
        /// Имя поста-пеленгатора
        /// </summary>
        [XmlAttribute(AttributeName = "sender")]
        public string Sender { get; set; }

        /// <summary>
        /// Имя поста-источника команды
        /// </summary>
        [XmlAttribute(AttributeName = "dest")]
        public string Dest { get; set; }

        /// <summary>
        /// Номер команды
        /// </summary>
        [XmlAttribute(AttributeName = "qdata")]
        public int Qdata { get; set; }

        /// <summary>
        /// 0
        /// </summary>
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }
}
