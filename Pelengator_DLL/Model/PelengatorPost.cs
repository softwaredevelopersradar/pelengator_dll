﻿namespace Pelengator_DLL
{
    /// <summary>
    /// Координаты пеленгатора
    /// </summary>
    public class PelengatorPost : IPelengatorPost
    {
        /// <summary>
        /// Широта пеленгатора (00:00:00.0N)
        /// </summary>
        public double Lat { get; set; }

        /// <summary>
        /// Долгота пеленгатора (00:00:00.0E)
        /// </summary>
        public double Lon { get; set; }

        /// <summary>
        /// Имя поста, на котором были получены результаты
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// ID
        /// </summary>
        public int Id { get; set; }
    }
}
