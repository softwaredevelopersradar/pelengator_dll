﻿namespace Pelengator_DLL
{

    /// <summary>
    /// Интрефейс координат пеленгатора
    /// </summary>
    public interface IPelengatorPost
    {
        /// <summary>
        /// Широта пеленгатора (00:00:00.0N)
        /// </summary>
        double Lat { get; set; }

        /// <summary>
        /// Долгота пеленгатора (00:00:00.0E)
        /// </summary>
        double Lon { get; set; }

        /// <summary>
        /// Имя поста, на котором были получены результаты
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// ID
        /// </summary>
        int Id { get; set; }
    }
}
