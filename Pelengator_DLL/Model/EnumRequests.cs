﻿namespace Pelengator_DLL
{
    /// <summary>
    /// Перечисление команд
    /// </summary>
    public enum PelengatorResponceType
    {
        /// <summary>
        /// Неизвестный запрос
        /// </summary>
        UNDEFINED,
        /// <summary>
        /// Регистрация
        /// </summary>
        REGISTRATION_RESPONSE,
        /// <summary>
        /// Пеленг
        /// </summary>
        PELENG_STARTED,
        /// <summary>
        /// Окончание разведки пеленга
        /// </summary>
        PELENG_EOF,
        /// <summary>
        /// Результат пеленга
        /// </summary>
        PELENG_RESULT,
        /// <summary>
        /// Отложенный пеленг
        /// </summary>
        DELAYED_PELENG_STARTED,
        /// <summary>
        /// Окончание разведки отложенного пеленга
        /// </summary>
        DELAYED_PELENG_EOF,
        /// <summary>
        /// Результат отложенного пеленга
        /// </summary>
        DELAYED_PELENG_RESULT,
        /// <summary>
        /// Результат Чпп
        /// </summary>
        FPP_RESULT
    }
}
