﻿using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Pelengator_DLL
{
    /// <summary>
    /// Класс конвертер String -> Unp, Unp -> String  
    /// </summary>
    public static class Translator
    {
        private static XmlSerializer _serializer = new XmlSerializer(typeof(Unp));

        /// <summary>
        /// Конвертер Unp -> String для отправки
        /// </summary>
        /// <param name="messageObject">Unp объект</param>
        /// <returns>Xml string</returns>
        public static string GetMessage(Unp messageObject)
        {
            var stringBuilder = new StringBuilder();

            var xmlWriterSettings = new XmlWriterSettings();

            var ns = new XmlSerializerNamespaces();
            ns.Add(string.Empty, string.Empty);
            
            var textWriter = XmlWriter.Create(stringBuilder, xmlWriterSettings);

            _serializer.Serialize(textWriter, messageObject, ns);

            stringBuilder.AppendLine();
            stringBuilder.Replace("utf-16", "windows-1251");

            return stringBuilder.ToString();
        }

        /// <summary>
        /// Конвертер String -> Unp после получения
        /// </summary>
        /// <param name="message">Xml string</param>
        /// <returns>Объект Unp</returns>
        public static Unp FromMessage(string message)
        {
            var fixedMessage = message.Trim().Replace("\"active=", "\" active="); // HOT FIX
            var leftBracketIndex = fixedMessage.IndexOf("<");// trim messages like this: Too many queries<?xml version="1.0" encoding="windows-1251"?><unp><ldftask ver="3" sender="server" dest="client" qdata="1163656192" id="0" Active="True"></ldftask></unp>

            if (leftBracketIndex < 0)
                return null;

            var reader = new StringReader(fixedMessage.Substring(leftBracketIndex));
            return (Unp)_serializer.Deserialize(reader);
        }
    }
}
