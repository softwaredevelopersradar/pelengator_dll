﻿using System;

namespace Pelengator_DLL
{
    public partial class PelengatorTcp
    {
        /// <summary>
        /// Indicates the byte order ("endianness") in which data is stored in this computer architecture.
        /// </summary>
        private bool IsBigEndian
        {
            get
            {
                return BitConverter.IsLittleEndian;
            }
        }

        /// <summary>
        /// Bytes order check
        /// </summary>
        /// <param name="array">Array to check</param>
        /// <returns>Final array</returns>
        private byte[] CheckBigEndian(byte[] array)
        {
            if (!IsBigEndian)
            {
                return array;
            }
            else
            {
                Array.Reverse(array);
                return array;
            }
        }

        /// <summary>
        ///    Copies a specified number of bytes from a source array starting at a particular
        ///    offset to a destination array starting at a particular offset.
        /// </summary>
        /// <param name="srcArr">The source buffer.</param>
        /// <param name="srcOffSet">The zero-based byte offset into src.</param>
        /// <param name="dstArr">The destination buffer.</param>
        /// <param name="dstOffSet">The zero-based byte offset into dst.</param>
        /// <param name="count">The number of bytes to copy.</param>
        private void ArrayCopy(byte[] srcArr, int srcOffSet, ref byte[] dstArr, int dstOffSet, int count)
        {
            Buffer.BlockCopy(srcArr, srcOffSet, dstArr, dstOffSet, count);
        }

        /// <summary>
        /// Converter to int32 with bytes order check
        /// </summary>
        /// <param name="array">Data array</param>
        /// <param name="position">Position from which to take the bytes</param>
        /// <returns>Final value</returns>
        private Int32 GetInt32(byte[] array, ref int position)
        {
            byte[] intBytes = new byte[4];
            ArrayCopy(array, position, ref intBytes, 0, 4);
            position += 4;
            return BitConverter.ToInt32(CheckBigEndian(intBytes), 0);
        }

        /// <summary>
        /// Converter to single with bytes order check
        /// </summary>
        /// <param name="array">Data array</param>
        /// <param name="position">Position from which to take the bytes</param>
        /// <returns>Final value</returns>
        private float GetSingle(byte[] array, ref int position)
        {
            byte[] intBytes = new byte[4];
            ArrayCopy(array, position, ref intBytes, 0, 4);
            position += 4;
            return BitConverter.ToSingle(CheckBigEndian(intBytes), 0);
        }

        /// <summary>
        /// Converter to short with bytes order check
        /// </summary>
        /// <param name="array">Data array</param>
        /// <param name="position">Position from which to take the bytes</param>
        /// <returns>Final value</returns>
        private short GetInt16(byte[] array, ref int position)
        {
            byte[] intBytes = new byte[2];
            ArrayCopy(array, position, ref intBytes, 0, 2);
            position += 2;
            return BitConverter.ToInt16(CheckBigEndian(intBytes), 0);
        }
    }
}
