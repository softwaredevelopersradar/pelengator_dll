﻿using System;

namespace Pelengator_DLL
{
    /// <summary>
    /// Класс окончания пеленга
    /// </summary>
    public class PelengEoF
    {
        /// <summary>
        /// Результат окончания
        /// </summary>
        public bool Done { get; set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="done">Результат</param>
        public PelengEoF(bool done)
        {
            this.Done = done;
        }
    }

    /// <summary>
    /// Класс результат пеленга
    /// </summary>
    public class PelengResult
    {
        /// <summary>
        /// Координаты с именем поста, на котором были получены результаты
        /// </summary>
        public IPelengatorPost Post { get; set; }

        /// <summary>
        /// Пеленг, гр.
        /// </summary>
        public double HDir { get; set; }

        /// <summary>
        /// Частота РПУ (Гц)
        /// </summary>
        public long Freq { get; set; }

        /// <summary>
        /// Время выполнения задачи в формате ДД.ММ.ГГГГ ЧЧ:ММ:СС
        /// </summary>
        public DateTime Time { get; set; }

        /// <summary>
        /// Эквивалент качества (достоверность пеленга за период пеленгования) (массив)
        /// </summary>
        public double Quality { get; set; }

        /// <summary>
        /// Уровень сигнала (дБ/мкВ)
        /// </summary>
        public double Pwr { get; set; }
        /// <summary>
        /// Угол места, гр.
        /// </summary>
        public double VDir { get; set; }

        /// <summary>
        /// Дальность, км
        /// </summary>
        public double Distance { get; set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="post">Координаты с именем поста, на котором были получены результаты</param>
        /// <param name="hDir">Пеленг, гр.</param>
        /// <param name="freq">Частота РПУ (Гц)</param>
        /// <param name="time">Время выполнения задачи в формате ДД.ММ.ГГГГ ЧЧ:ММ:СС</param>
        /// <param name="quality">Эквивалент качества (достоверность пеленга за период пеленгования) (массив)</param>
        /// <param name="pwr">Уровень сигнала (дБ/мкВ)</param>
        /// <param name="vDir">Угол места, гр.</param>
        /// <param name="distance">Дальность, км</param>
        public PelengResult(IPelengatorPost post, double hDir, long freq, DateTime time, double quality,
                                         double pwr, double vDir, double distance)
        {
            this.Post = post;
            this.HDir = hDir;
            this.Freq = freq;
            this.Time = time;
            this.Quality = quality;
            this.Pwr = pwr;
            this.VDir = vDir;
            this.Distance = distance;
        }
    }
}
