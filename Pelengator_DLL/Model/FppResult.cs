﻿using System;

namespace Pelengator_DLL
{
    struct Response
    {
        public Int32 answerCode { get; set; }
        public double startRangeFreq { get; set; }
        public double endRangeFreq { get; set; }
        public Int32 taskID { get; set; }
        public double startFreq { get; set; }
        public double step { get; set; }
        public Int32 count { get; set; }
        public Int32 size { get; set; }
        public DataStruct data { get; set; }

        public Response(int answerCode, double startRangeFreq, double endRangeFreq, int taskID, double startFreq, double step, int count, int size, DataStruct data) : this()
        {
            this.answerCode = answerCode;
            this.startRangeFreq = startRangeFreq;
            this.endRangeFreq = endRangeFreq;
            this.taskID = taskID;
            this.startFreq = startFreq;
            this.step = step;
            this.count = count;
            this.size = size;
            this.data = data;
        }
    }


    struct ArrayByte
    {
        public Int32 size;
        public byte[] arr { get; set; }

        public ArrayByte(int size, byte[] arr) : this()
        {
            this.size = size;
            this.arr = arr;
        }
    }

    struct ArrayShort
    {
        public Int32 size;
        public short[] arr { get; set; }

        public ArrayShort(int size, short[] arr) : this()
        {
            this.size = size;
            this.arr = arr;
        }
    }

    struct DataStruct
    {
        public ArrayByte busyPercentages { get; set; }
        public ArrayByte levels { get; set; }
        public ArrayShort azimuth { get; set; }
        public ArrayByte threshold { get; set; }

        public DataStruct(ArrayByte busyPercentages, ArrayByte levels, ArrayShort azimuth, ArrayByte threshold) : this()
        {
            this.busyPercentages = busyPercentages;
            this.levels = levels;
            this.azimuth = azimuth;
            this.threshold = threshold;
        }
    }
}
