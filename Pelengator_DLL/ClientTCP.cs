﻿using SimpleTcp;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pelengator_DLL
{
    public partial class PelengatorTcp : PelengatorModel
    {
        /// <summary>
        /// The client.
        /// </summary>
        private SimpleTcpClient client;

        /// <summary>
        /// Connected client socket
        /// </summary>
        public override event EventHandler<string> OnConnect;

        /// <summary>
        /// Disconnected client socket
        /// </summary>
        public override event EventHandler<string> OnDisconnect;

        /// <summary>
        /// Initializes a new instance of the <see cref="PelengatorTcp"/> class.
        /// </summary>
        /// <param name="ip">
        /// IP for Tcp.
        /// </param>
        /// <param name="port">
        /// Port for Tcp.
        /// </param>
        public PelengatorTcp(string ip, int port, int lengthBuffer)
        {
            this.client = new SimpleTcpClient(ip, port);
            this.Ip = ip;
            this.Port = port;
            this.LengthBuffer = lengthBuffer;
        }

        /// <summary>
        /// Gets or sets the IP.
        /// </summary>
        public string Ip { get; private set; }

        /// <summary>
        /// Gets or sets the port.
        /// </summary>
        public int Port { get; private set; }

        /// <summary>
        /// Gets or sets lengthOfBuffer.
        /// </summary>
        public int LengthBuffer { get; private set; }

        /// <summary>
        /// The connect.
        /// </summary>
        /// <param name="token">
        /// <see cref="CancellationToken"/>
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override async Task<bool> Connect(CancellationToken token = default)
        {
            try
            {
                var connectTask = await Task.Run(() =>
                {
                    client = new SimpleTcpClient(this.Ip, this.Port);
                    SubscribeToEvents();

                    this.client.Connect();
                    this.client.Settings.StreamBufferSize = 65535;
                    return true;
                }, token);

                return connectTask;
            }
            catch
            {
                this.client.Dispose();
                return false;
            }
        }

        /// <summary>
        /// Disconnect Tcp.
        /// </summary>
        /// <returns>
        /// <param name="token">
        /// <see cref="CancellationToken"/>
        /// </param>
        /// The <see cref="bool"/>.
        /// </returns>
        public override async Task<bool> Disconnect(CancellationToken token = default)
        {
            try
            {
                var connectTask = await Task.Run(() =>
                {
                    UnsubscribeToEvents();
                    this.client.Disconnect();

                    return true;
                }, token);

                return connectTask;
            }
            catch
            {
                return false;
            }
        }

        private void SubscribeToEvents()
        {
            //this.client.Events.DataReceived += (sender, args) =>
            //{
            //    WaitAndDecode(args.Data);
            //};

            //this.client.Events.Connected += (sender, args) =>
            //{
            //    OnConnect?.Invoke(this, args.IpPort);
            //};

            //this.client.Events.Disconnected += (sender, args) =>
            //{
            //    OnDisconnect?.Invoke(this, args.IpPort);
            //};
        }

        private void UnsubscribeToEvents()
        {
            //this.client.Events.DataReceived -= (sender, args) =>
            //{
            //    WaitAndDecode(args.Data);
            //};

            //this.client.Events.Connected -= (sender, args) =>
            //{
            //    OnConnect?.Invoke(this, args.IpPort);
            //};

            //this.client.Events.Disconnected -= (sender, args) =>
            //{
            //    OnDisconnect?.Invoke(this, args.IpPort);
            //};
        }
    }
}
