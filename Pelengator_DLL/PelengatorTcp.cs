﻿using KvetkaDataModels.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Pelengator_DLL
{
    using System.Diagnostics;

    using DataReceivedEventArgs = SimpleTcp.DataReceivedEventArgs;

    /// <summary>
    /// TCP type connection.
    /// </summary>
    public partial class PelengatorTcp : PelengatorModel, IDisposable
    {
        /// <summary>
        /// Кодировка символов
        /// </summary>
        private Encoding _encoding = Encoding.UTF8;

        private readonly List<PelengatorPost> _posts = new List<PelengatorPost>();
        /// <summary>
        /// Коллекция координат и имен хостов
        /// </summary>
        public List<PelengatorPost> Posts
        {
            get { return this._posts; }
        }
        
        /// <summary>
        /// Bytes sent via TCP.
        /// </summary>
        public override event EventHandler<byte[]> OnWrite;

        /// <summary>
        /// Bytes read via Tcp.
        /// </summary>
        public override event EventHandler<byte[]> OnRead;

        /// <summary>
        /// Запрос «регистрации»
        /// </summary>
        /// <param name="hostname">Имя регистрируемого поста</param>
        /// <param name="password">Пароль</param>
        /// <param name="token">
        /// <see cref="CancellationToken"/>
        /// </param>
        /// <returns>Результат регистрации</returns>
        public async Task<Unp> Registration(string hostname, string password, CancellationToken token = default)
        {
            Unp pelengatorResponce = new Unp();
            try
            {
                ConnectMessage connectRequestMessage = new ConnectMessage { Hostname = hostname, Password = password };
                Unp connectMessageRequest = new Unp { UnpRegister = connectRequestMessage };

                pelengatorResponce = await this.WaitAndDecode(connectMessageRequest, PelengatorResponceType.REGISTRATION_RESPONSE, token).ConfigureAwait(false);

            }
            catch
            {
                return pelengatorResponce;
            }

            return pelengatorResponce;
        }

        /// <summary>
        /// Запрос «ЧПП»
        /// </summary>
        /// <param name="sender">Имя поста отправителя</param>
        /// <param name="dest">Имя поста, выполняющего задание</param>
        /// <param name="token">
        /// <see cref="CancellationToken"/>
        /// </param>
        /// <returns>Результат регистрации</returns>
        public async Task<Fpp> FppRequest(string sender, string dest, CancellationToken token = default)
        {
            Unp fppResponce = new Unp();
            Fpp FppResult = new Fpp();

            Guid requestGuid = Guid.NewGuid();
            int requestId = Math.Abs(requestGuid.GetHashCode());

            try
            {
                Fppftask fppRequestMessage = new Fppftask { Sender = sender, Dest = dest, Qdata = 1 };
                Unp fppMessageRequest = new Unp { Fppftask = fppRequestMessage };
                fppResponce = await this.WaitAndDecode(fppMessageRequest, PelengatorResponceType.FPP_RESULT, token).ConfigureAwait(false);
                
                if (fppResponce != null)
                {
                    fppResponce.Fppresult.Dir.AzsArr = Parse<double>(fppResponce.Fppresult.Dir.Azs, true);
                    fppResponce.Fppresult.Dir.LevelsArr = Parse<double>(fppResponce.Fppresult.Dir.Levels, true);
                    if (fppResponce.Fppresult.Dir.BFreq != 0 && fppResponce.Fppresult.Dir.EFreq != 0)
                    {
                        FppResult = new Fpp(fppResponce.Fppresult.Dir.Id,
                            new FrequencyRange(fppResponce.Fppresult.Dir.BFreq, fppResponce.Fppresult.Dir.EFreq, fppResponce.Fppresult.Dir.EFreq - fppResponce.Fppresult.Dir.BFreq),
                            0, fppResponce.Fppresult.Dir.Step, null, null, fppResponce.Fppresult.Dir.LevelsArr, fppResponce.Fppresult.Dir.AzsArr);
                    }

                    return await Task.FromResult(FppResult);
                }

                return FppResult;
            }
            catch(Exception ex)
            {
                return FppResult;
            }
        }

        /// <summary>
        /// Запрос «ЧПП»
        /// </summary>
        /// <param name="sFreq">Начальная частота панорамы</param>
        /// <param name="eFreq">Конечная частота панорамы</param>
        /// <param name="token">
        /// <see cref="CancellationToken"/>
        /// </param>
        /// <returns>Результат ЧПП</returns>
        public async Task<Fpp> FppRequest(float sFreq, float eFreq, CancellationToken token = default)
        {
            Fpp FppResult = new Fpp();
            try
            {
                byte[] data = new byte[17];
                data[0] = 22;
                Buffer.BlockCopy(BitConverter.GetBytes((double)sFreq).Reverse().ToArray(), 0, data, 1, 8);
                Buffer.BlockCopy(BitConverter.GetBytes((double)eFreq).Reverse().ToArray(), 0, data, 9, 8);

                FppResult = await this.WaitAndDecodeBin(data, token).ConfigureAwait(false);

                return FppResult;
            }
            catch(Exception ex)
            {
                return FppResult;
            }
        }

        /// <summary>
        /// Команда на пеленгование 
        /// </summary>
        /// <param name="sender">Имя поста-отправителя</param>
        /// <param name="dest">Имя поста-пеленгатора</param>
        /// <param name="freqHz">Частота (Гц) (100 000 – 30 000 000)</param>
        /// <param name="bandWidth">Полоса (Гц) (300 – 12 000)</param>
        /// <param name="token">
        /// <see cref="CancellationToken"/>
        /// </param>
        /// <returns>Результат пеленга</returns>
        public async Task<Bearing> Bearing(string sender, string dest, long freqHz, int bandWidth, CancellationToken token = default)
        {
            Unp pelengatorResponce = new Unp();
            Bearing bearingResult = new Bearing();

            Guid requestGuid = Guid.NewGuid();
            int requestId = Math.Abs(requestGuid.GetHashCode());

            Band freq = new Band { Base = freqHz, Width = bandWidth };

            Freqs freqs = new Freqs();

            freqs.Band = new List<Band>
            {
                    freq
            };

            Unp requestMessage = new Unp { Dftask = new Dftask { Sender = sender, Dest = dest, Qdata = requestId, Freqs = freqs} };

            pelengatorResponce = await this.WaitAndDecode(requestMessage, PelengatorResponceType.PELENG_RESULT, token).ConfigureAwait(false);

            if (pelengatorResponce == null)
            {
                return bearingResult;
            }

            Dir dir = pelengatorResponce.Dfresult.Dir.FirstOrDefault();

            PelengatorPost post = GetOrCreatePost(dir);

            DateTime time;
            if (!DateTime.TryParse(dir.Time, out time))
                time = DateTime.Now;

            bearingResult = new Bearing(post.Lat, post.Lon, Parse<double>(dir.Hdir), new FrequencyRange(dir.Freq, bandWidth), time, Parse<double>(dir.Qual), Parse<double>(dir.Pwr),
                                                                                                                        Parse<double>(dir.Vdir), Parse<double>(dir.Dist));
            return bearingResult;
        }

        /// <summary>
        /// Команда на отложенное пеленгование 
        /// </summary>
        /// <param name="sender">Имя поста-отправителя</param>
        /// <param name="dest">Имя поста-пеленгатора</param>
        /// <param name="freqHz">Частота (Гц) (100 000 – 30 000 000)</param>
        /// <param name="bandWidth">Полоса (Гц) (300 – 12 000)</param>
        /// <param name="timePeleng">Время пеленгования (отложенный режим) в формате ДД.ММ.ГГГГ ЧЧ:ММ:СС</param>
        /// <param name="period">Длительность в секундах</param>
        /// <param name="showSpectr">0/1 – передавать спектр</param>
        /// <param name="all_iri">0/1 – передавать один результат, передавать таблицу ИРИ</param>
        /// <param name="dynamic">0/1 – передавать динамику параметров</param>
        /// <param name="token">
        /// <see cref="CancellationToken"/>
        /// </param>
        /// <returns>Результат пеленга</returns>
        public async Task<DelayedBearing> DelayedBearing(string sender, string dest, long freqHz, int bandWidth, DateTime timePeleng, double period, bool showSpectr, bool all_iri, bool dynamic, CancellationToken token = default)
        {
            Unp pelengatorResponce = new Unp();
            DelayedBearing bearingResult = new DelayedBearing();

            Guid requestGuid = Guid.NewGuid();
            int requestId = Math.Abs(requestGuid.GetHashCode());

            Band freq = new Band { Base = freqHz, Width = bandWidth, Time = timePeleng.ToString(PelengatorConts.DateTimeFormat), Period = period, Spectr = showSpectr ? 1 : 0, All_iri = all_iri ? 1 : 0, Dynamic = dynamic ? 1 : 0 };

            Freqs freqs = new Freqs();

            freqs.Band = new List<Band>
            {
                    freq
            };

            Unp requestMessage = new Unp { Ldftask = new Ldftask { Sender = sender, Dest = dest, Qdata = requestId, Freqs = freqs } };

            pelengatorResponce = await this.WaitAndDecode(requestMessage, PelengatorResponceType.DELAYED_PELENG_RESULT, token).ConfigureAwait(false);

            if (pelengatorResponce == null)
            { 
                return bearingResult;
            }

            Dir dir = pelengatorResponce.Ldfresult.Dir.FirstOrDefault();

            PelengatorPost post = GetOrCreatePost(dir);

            DateTime time;
            if (!DateTime.TryParse(dir.Time, out time))
                time = DateTime.Now;

            bearingResult.PostName = dir.Post;
            bearingResult.Freq = new FrequencyRange(dir.Freq, bandWidth);
            bearingResult.Time = time;
            bearingResult.Lat = post.Lat;
            bearingResult.Lon = post.Lon;
            bearingResult.Quality = Parse<double>(dir.Qual, true);
            bearingResult.Hdir = Parse<double>(dir.Hdir, true);
            bearingResult.HdirSD = Parse<double>(dir.Hdir_sko, true);
            bearingResult.Vdir = Parse<double>(dir.Vdir, true);
            bearingResult.VdirSD =  Parse<double>(dir.Vdir_sko, true);
            bearingResult.Power = Parse<double>(dir.Pwr, true);
            bearingResult.Dist = Parse<double>(dir.Dist, true);
            bearingResult.SignalClass = dir.Klass;
            bearingResult.SingalScc = dir.Scc;
            bearingResult.HdirDyn = Parse<double>(dir.Hdir_dyn, true);
            bearingResult.VdirDyn = Parse<double>(dir.Vdir_dyn, true);
            bearingResult.PowerDyn = Parse<double>(dir.Pwr_dyn, true);
            bearingResult.TimeDyn = ParseTime(dir.Time_dyn);
            bearingResult.StartFreq = Convert.ToDouble(dir.Spectr_f0);
            bearingResult.FreqStep = Convert.ToDouble(dir.Spectr_step);
            bearingResult.LevelArray = Parse<double>(dir.Spectr_u, true);
            return bearingResult;
        }

        /// <summary>
        /// Send message.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="token">
        /// <see cref="CancellationToken"/>
        /// </param>
        protected override async Task Send(Unp message, CancellationToken token = default)
        {
            try
            {
                await Task.Delay(5);
                if (client != null)
                {
                    string connectMessageRequest = Translator.GetMessage(message);
                    byte[] requestData = this._encoding.GetBytes(connectMessageRequest);
                    await client.SendAsync(requestData, token);
                    OnWrite?.Invoke(this, requestData);
                }
            }
            catch
            {
                // ignored
            }
        }

        /// <summary>
        /// Send FPP request
        /// </summary>
        /// <param name="message">FPP message</param>
        /// <param name="token"><see cref="CancellationToken"/></param>
        protected override async Task Send(byte[] message, CancellationToken token = default)
        {
            if(client != null)
            {
                await Task.Delay(5);
                await client.SendAsync(message, token);
                OnWrite?.Invoke(this, message);
            }
        }

        private async Task<Unp> WaitAndDecode(Unp unp, PelengatorResponceType responceType, CancellationToken token = default)
        {
            using (MemoryStream ms = new MemoryStream(this.LengthBuffer))
            {
                this.client.Events.DataReceived += OnEvents_OnDataReceived;
                await this.Send(unp, token).ConfigureAwait(false);
                try
                {
                    do
                    {
                        await Task.Delay(1, token).ConfigureAwait(false);
                    }
                    while (!this.Deserialize<object>(ms, responceType) && !token.IsCancellationRequested); // пока данные есть в потоке
                }
                finally
                {
                    this.client.Events.DataReceived -= OnEvents_OnDataReceived;
                }

                return this.Decoder(ms.ToArray(), responceType);

                void OnEvents_OnDataReceived(object sender, DataReceivedEventArgs args) =>
                    ms.Write(args.Data, 0, args.Data.Length);
            }

        }

        private async Task<Fpp> WaitAndDecodeBin(byte[] message, CancellationToken token = default)
        {
            using (MemoryStream ms = new MemoryStream(this.LengthBuffer))
            {
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                this.client.Events.DataReceived += OnEvents_OnDataReceived;
                await this.Send(message, token).ConfigureAwait(false);
                try
                {
                    do
                    {
                        await Task.Delay(1).ConfigureAwait(false);
                        if (stopwatch.ElapsedMilliseconds > 5000)
                        {
                            break;
                        }
                    }
                    while (!false); // пока данные есть в потоке
                }
                finally
                {
                    this.client.Events.DataReceived -= OnEvents_OnDataReceived;

                }

                return DecodeFpp(ms.ToArray());

                void OnEvents_OnDataReceived(object sender, DataReceivedEventArgs args)
                {
                    ms.Write(args.Data, 0, args.Data.Length);
                    stopwatch.Restart();
                }
            }

        }

        private bool Deserialize<T>(MemoryStream ms, PelengatorResponceType unp)
        {
            var marker = unp != PelengatorResponceType.PELENG_RESULT ? "</unp>" : "</dfresult></unp>";
            return ms.Length != 0 && this._encoding.GetString(ms.ToArray()).Contains(marker);
        }

        /// <summary>
        /// Decoding the response
        /// </summary>
        /// <param name="buffer">Received array</param>
        private Unp Decoder(byte[] buffer, PelengatorResponceType responceType)
        {
            OnRead?.Invoke(this, buffer);

            StringBuilder xmlBuffer = new StringBuilder();

            if (buffer.Length > 0)
            {
                if (responceType != PelengatorResponceType.PELENG_RESULT)
                {
                    xmlBuffer.Append(this._encoding.GetString(buffer, 0, buffer.Length));
                }
                else
                {
                    xmlBuffer = GetBearingResult(buffer);
                    if (xmlBuffer == null)
                        return null;
                }

                int ancorFlag = 0;
                int cutPosition = 0;

                for (int i = 0; i < xmlBuffer.Length; i++)
                {
                    char currSimbol = xmlBuffer[i];

                    if (currSimbol == '<')
                    {
                        ancorFlag++;
                    }
                    else if (currSimbol == '>')
                    {
                        ancorFlag--;
                    }

                    if (i <= 6)
                    {
                        continue;
                    }

                    if (new string(
                            new char[]
                                {
                                    xmlBuffer[i - 5], xmlBuffer[i - 4], xmlBuffer[i - 3], xmlBuffer[i - 2],
                                    xmlBuffer[i - 1], xmlBuffer[i]
                                }) == PelengatorConts.XmlEofMarker && ancorFlag == 0)
                    {
                        Unp unpObject = Translator.FromMessage(cutPosition > 0 ? xmlBuffer.ToString(cutPosition + 1, i - cutPosition) :
                        xmlBuffer.ToString(cutPosition, i - cutPosition + 1));
                        
                        if (unpObject.UnpRegister == null
                            && unpObject.Dfresult == null
                            && unpObject.Ldfresult == null
                            && unpObject.Fppresult == null)
                        {
                            cutPosition = i;
                            continue;
                        }

                        return unpObject;
                        //AddToPool(unpObject);

                    }
                }

                if (cutPosition > 0)
                {
                    xmlBuffer.Remove(0, cutPosition + 1);
                }
            }

            return null;
        }

        public StringBuilder GetBearingResult(byte[] buffer)
        {
            string xmlString = _encoding.GetString(buffer, 0, buffer.Length);
            int countOfUnp = Regex.Matches(xmlString, "</unp>").Count;

            if (countOfUnp == 1)
            {
                return new StringBuilder(xmlString);
            }

            int LastIndexOfFirstUnp = xmlString.IndexOf("</unp>") + 6;
            int LastIndexOfSecondUnp = xmlString.IndexOf("</dfresult></unp>") + 17;

            if (LastIndexOfFirstUnp == -1 || LastIndexOfSecondUnp == -1)
            {
                return null;
            }

            if (countOfUnp == 2)
            {
                xmlString = xmlString.Substring(LastIndexOfFirstUnp, LastIndexOfSecondUnp - LastIndexOfFirstUnp);
                return new StringBuilder(xmlString);
            }
            else if (countOfUnp == 3)
            {
                xmlString = xmlString.Substring(LastIndexOfFirstUnp,
                      LastIndexOfSecondUnp - LastIndexOfFirstUnp);
                return new StringBuilder(xmlString);
            }

            return null;
        }

        private Fpp DecodeFpp(byte[] buffer)
        {
            Fpp fppResult = new Fpp();
            Response myParse = new Response();
            DataStruct myDataParse = new DataStruct();
            try
            {
                #region try parse to my class
                int position = 1;

                if (buffer[0] != 22)
                {
                    return fppResult;
                }

                myParse.answerCode = 22;
                myParse.startRangeFreq = GetSingle(buffer, ref position);
                myParse.endRangeFreq = GetSingle(buffer, ref position);
                myParse.taskID = GetInt32(buffer, ref position);
                myParse.startFreq = GetSingle(buffer, ref position);
                myParse.step = GetSingle(buffer, ref position);
                myParse.count = GetInt32(buffer, ref position);
                myParse.size = GetInt32(buffer, ref position);

                if (myParse.size == 0)
                {
                    fppResult = new Fpp(myParse.taskID, new FrequencyRange((float)myParse.startRangeFreq * 1000,
                    (float)myParse.endRangeFreq * 1000, 0), (float)myParse.startFreq, (float)myParse.step,
                    null, null,
                    null, null);

                    return fppResult;
                }

                int masSize = GetInt32(buffer, ref position);
                myDataParse.busyPercentages = new ArrayByte(masSize, new byte[masSize]);
                Buffer.BlockCopy(buffer, position, myDataParse.busyPercentages.arr, 0, masSize);
                position += masSize;

                masSize = GetInt32(buffer, ref position);
                myDataParse.levels = new ArrayByte(masSize, new byte[masSize]);
                Buffer.BlockCopy(buffer, position, myDataParse.levels.arr, 0, masSize);
                position += masSize;

                masSize = GetInt32(buffer, ref position); ;
                myDataParse.azimuth = new ArrayShort(masSize, new short[masSize]);

                for (int i = 0; i < myDataParse.azimuth.arr.Length; i ++)
                {
                    myDataParse.azimuth.arr[i] = (short)(GetInt16(buffer, ref position) / 10);
                }

                masSize = GetInt32(buffer, ref position);
                myDataParse.threshold = new ArrayByte(masSize, new byte[masSize]);
                Buffer.BlockCopy(buffer, position, myDataParse.threshold.arr, 0, masSize);
                #endregion

                fppResult = new Fpp(myParse.taskID, new FrequencyRange((float)myParse.startRangeFreq * 1000,
                    (float)myParse.endRangeFreq * 1000, 0), (float)myParse.startFreq, (float)myParse.step,
                    myDataParse.busyPercentages.arr, myDataParse.threshold.arr,
                    myDataParse.levels.arr.Select(x => Convert.ToDouble(x)).ToArray(), myDataParse.azimuth.arr.Select(x => Convert.ToDouble(x)).ToArray());

                return fppResult;
            }
            catch(Exception ex)
            {
                return fppResult;
            }
        }

        /// <summary>
        /// Конвертер string -> T[]
        /// </summary>
        /// <typeparam name="T">Тип данных</typeparam>
        /// <param name="part">Данные</param>
        /// <param name="arr">Если парсим массив</param>
        /// <returns>T[]</returns>
        private T[] Parse<T>(string part, bool arr) where T : IConvertible
        {
            Type type = typeof(T);

            if (string.IsNullOrEmpty(part))
            {
                return null;
            }

            part = part.Replace(",", ".");
            string[] elements = part.Split(';');
            T[] result = new T[elements.Length];

            for (int i = 0; i < elements.Length; i++)
            {
                result[i] = (T)Convert.ChangeType(elements[i], type, CultureInfo.InvariantCulture);
            }

            return result;
        }

        /// <summary>
        /// Конвертер string -> DateTime[]
        /// </summary>
        /// <param name="time">Время выполнения задач</param>
        /// <returns>Массив времени</returns>
        private DateTime[] ParseTime(string time)
        {
            if (string.IsNullOrEmpty(time))
            {
                return null;
            }

            string[] elements = time.Split(';');
            DateTime[] result = new DateTime[elements.Length];

            string format = "dd.MM.yyyy HH:mm:ss.fff";

            for (int i = 0; i < elements.Length; i++)
            {
                result[i] = DateTime.ParseExact(elements[i], format, CultureInfo.InvariantCulture);
            }

            return result;
        }

        /// <summary>
        /// Конвертер string -> T[]
        /// </summary>
        /// <typeparam name="T">Тип данных</typeparam>
        /// <param name="part">Данные</param>
        /// <returns>T[]</returns>
        private T Parse<T>(string part) where T : IConvertible
        {
            Type type = typeof(T);
            part = part.Replace(",", ".");
            return (T)Convert.ChangeType(part, type, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Despose TCP client
        /// </summary>
        public void Dispose()
        {
            client?.Dispose();
        }

        /// <summary>
        /// Получение координат и имени хоста
        /// </summary>
        /// <param name="dir">Данные</param>
        /// <returns></returns>
        private PelengatorPost GetOrCreatePost(Dir dir)
        {
            double lat = ParseLocation(dir.Lat);
            double lon = ParseLocation(dir.Lon);

            lock (Posts)
            {
                PelengatorPost postModel = Posts.FirstOrDefault(x => x.Name == dir.Post && Math.Abs(x.Lat - lat) < 0.00001 && Math.Abs(x.Lon - lon) < 0.00001);
                if (postModel == null)
                {
                    postModel = new PelengatorPost { Lat = lat, Lon = lon, Name = dir.Post, Id = Posts.Count };
                    Posts.Add(postModel);
                }

                return postModel;
            }
        }

        /// <summary>
        /// String coord -> double coord
        /// </summary>
        /// <param name="latOrLon">String coord</param>
        /// <returns>Double coord</returns>
        public double ParseLocation(string latOrLon)
        {
            // 59:43:17.0N 30:02:08.9E
            Regex regex = new Regex(@"(\d+):(\d+):(\d+).(\d+)");
            Match latMatches = regex.Match(latOrLon);

            double doubleVal = double.Parse(string.Format($"{latMatches.Groups[1].Value}.{latMatches.Groups[2].Value}{latMatches.Groups[3].Value}{latMatches.Groups[4].Value}"), CultureInfo.InvariantCulture);

            return doubleVal;
        }
    }

}

