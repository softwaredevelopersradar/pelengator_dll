﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pelengator_DLL
{
    /// <summary>
    /// Base class for connect to pelengator.
    /// </summary>
    public abstract class PelengatorModel
    {

        #region Event`s

        /// <summary>
        /// Bytes read via TCP.
        /// </summary>
        public abstract event EventHandler<byte[]> OnRead;

        /// <summary>
        /// Bytes sent via TCP.
        /// </summary>
        public abstract event EventHandler<byte[]> OnWrite;

        /// <summary>
        /// Connected client socket
        /// </summary>
        public abstract event EventHandler<string> OnConnect;

        /// <summary>
        /// Disconnected client socket
        /// </summary>
        public abstract event EventHandler<string> OnDisconnect;

        #endregion

        /// <summary>
        /// Open TCP type connection
        /// </summary>
        /// <param name="token">The <see cref="CancellationToken"/></param>
        /// <returns>True - connect, False - disconnect</returns>
        public abstract Task<bool> Connect(CancellationToken token = default);

        /// <summary>
        /// Disconnect TCP
        /// </summary>
        /// <param name="token">The <see cref="CancellationToken"/></param>
        /// <returns>True - connect, False - disconnect</returns>
        public abstract Task<bool> Disconnect(CancellationToken token = default);

        /// <summary>
        /// Used by commands, implemented separately for each type of connection 
        /// </summary>
        /// <param name="message">Command for Send</param>
        /// <param name="token">The <see cref="CancellationToken"/></param>
        protected abstract Task Send(Unp message, CancellationToken token = default);
        /// <summary>
        /// Used by command FPP, implemented separately for each type of connection 
        /// </summary>
        /// <param name="message">FPP request</param>
        /// <param name="token">The <see cref="CancellationToken"/></param>
        protected abstract Task Send(byte[] message, CancellationToken token = default);
    }
}
